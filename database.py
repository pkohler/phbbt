import os
import schema
import sqlalchemy as sqla
import sqlalchemy.orm as orm


class PhbbtDB:

    def __init__(self, filename=None):
        if not filename:
            filename = 'sqlite:///phbbt.db'
        self.engine = sqla.create_engine(filename)
        schema.Base.metadata.bind = self.engine
        self.session = orm.sessionmaker(bind=self.engine)()
        self.table_map = {
            'jobs': schema.PlayerClass,
            'spells': schema.Spell,
            'races': schema.Race,
            'tools': schema.Tool,
            'skills': schema.Skill,
            'weapons': schema.WeaponType,
            'armor': schema.ArmorType,
            'feats': schema.Feat,
            'languages': schema.Language,
            'features': schema.RacialFeature
            }
        self.field_map = {
            'spells': {
                'name': schema.Spell.name,
                'level': schema.Spell.level,
                'casters': (schema.Spell.casters, schema.PlayerClass.name),
                'school': schema.Spell.school,
                'range': schema.Spell.range,
                'casting time': schema.Spell.casting_time,
                'duration': schema.Spell.duration,
                'description': schema.Spell.description,
                'scaling': schema.Spell.scaling,
                'ritual': schema.Spell.ritual
                },
            'jobs': {
                'name': schema.PlayerClass.name,
                'hd': schema.PlayerClass.hd,
                'spellcasting': schema.PlayerClass.spellcasting,
                'spellcasting ability': schema.PlayerClass.spellcasting_ability,
                'tool count': schema.PlayerClass.tool_choices,
                'tool options': (schema.PlayerClass.proficiency_tools, schema.Tool.name),
                'class points': schema.PlayerClass.class_points,
                'class point name': schema.PlayerClass.class_point_name,
                'saves': (schema.PlayerClass.proficiency_saves, schema.AbilityScore.name),
                'weapons': (schema.PlayerClass.proficiency_weapons, schema.WeaponType.name),
                'armor': (schema.PlayerClass.proficiency_armor, schema.ArmorType.name),
                'skills': (schema.PlayerClass.proficiency_skills, schema.Skill.name)
                },
            'races': {
                'name': schema.Race.name,
                'str': schema.Race.bonus_str,
                'dex': schema.Race.bonus_dex,
                'con': schema.Race.bonus_con,
                'cha': schema.Race.bonus_cha,
                'wis': schema.Race.bonus_wis,
                'int': schema.Race.bonus_int,
                'choice': schema.Race.bonus_choice,
                'speed': schema.Race.speed,
                'size': schema.Race.size,
                'features': (schema.Race.features, schema.RacialFeature.name),
                'languages': (schema.Race.languages, schema.Language.name),
                'skills': (schema.Race.proficiency_skills, schema.Skill.name),
                'tools': (schema.Race.proficiency_tools, schema.Tool.name),
                'weapons': (schema.Race.proficiency_weapons, schema.WeaponType.name),
                'armor': (schema.Race.proficiency_armor, schema.ArmorType.name)
                },
            'tools': {'name': schema.Tool.name},
            'skills': {'name': schema.Skill.name, 'ability': schema.Skill.ability},
            'weapons': {'name': schema.WeaponType.name},
            'armor': {'name': schema.ArmorType.name},
            'feats': {
                'name': schema.Feat.name,
                'ability_number': schema.Feat.ability_number,
                'ability_choices': (schema.Feat.ability_choices, schema.AbilityScore.name),
                'prerequisite': schema.Feat.prerequisite,
                'description': schema.Feat.description
                },
            'language': {'name': schema.Language.name, 'alphabet': schema.Language.alphabet}
        }

    def insert_classes(self):
        from phb.jobs import classes
        for c in sorted(classes):
            print('adding player class: %s' % c)
            pc = schema.PlayerClass(
                name=c,
                hd=classes[c]['hd'],
                skill_choices=classes[c]['skill_choices'],
                spellcasting=classes[c]['spellcasting'],
                spellcasting_ability=classes[c]['spellcasting_ability'],
                tool_choices=classes[c]['tool_choices'],
                class_points=classes[c]['class_points']
                )
            if classes[c]['class_points']:
                pc.class_point_name = classes[c]['class_point_name']
            for p in classes[c]['proficiency_skills']:
                prof = self.session.query(schema.Skill).filter(schema.Skill.name == p).one()
                pc.proficiency_skills.append(prof)
            for p in classes[c]['proficiency_tools']:
                prof = self.session.query(schema.Tool).filter(schema.Tool.name == p).one()
                pc.proficiency_tools.append(prof)
            for p in classes[c]['proficiency_weapons']:
                prof = self.session.query(schema.WeaponType).filter(schema.WeaponType.name == p).one()
                pc.proficiency_weapons.append(prof)
            for p in classes[c]['proficiency_armor']:
                prof = self.session.query(schema.ArmorType).filter(schema.ArmorType.name == p).one()
                pc.proficiency_armor.append(prof)
            for p in classes[c]['saving_throws']:
                prof = self.session.query(schema.AbilityScore).filter(schema.AbilityScore.name == p).one()
                pc.proficiency_saves.append(prof)
            self.session.add(pc)
        self.session.commit()

    def insert_spells(self):
        from phb.spells import spells
        for k in sorted(spells):
            print('adding spell: %s' % k)
            spell = schema.Spell(
                name=k,
                level=spells[k]['level'],
                school=spells[k]['school'],
                ritual=spells[k]['ritual'],
                range=spells[k]['range'],
                components=spells[k]['components'],
                duration=spells[k]['duration'],
                scaling=spells[k]['scaling'],
                description=spells[k]['description'],
                casting_time=spells[k]['casting_time']
                )
            if spells[k]['classes'] != []:
                for c in spells[k]['classes']:
                    caster = self.session.query(schema.PlayerClass).filter(schema.PlayerClass.name == c).one()
                    spell.casters.append(caster)
            self.session.add(spell)
        self.session.commit()

    def insert_feats(self):
        from phb.feats import feats
        for f in sorted(feats):
            print('adding feat: %s' % f)
            feat = schema.Feat(
                name=f,
                ability_number=feats[f]['ability_number'],
                description=feats[f]['description'],
                prerequisite=feats[f]['prerequisite']
                )
            for a in feats[f]['ability_choices']:
                ability = self.session.query(schema.AbilityScore).filter(schema.AbilityScore.name == a).one()
                feat.ability_choices.append(ability)
            self.session.add(feat)
        self.session.commit()

    def insert_tools(self):
        from phb.tool_armor_weapon import tools
        for t in tools:
            tool = schema.Tool(name=t)
            self.session.add(tool)
        self.session.commit()

    def insert_racial_features(self):
        from phb.racial_features import racial_features
        for f in racial_features:
            r = schema.RacialFeature(
                name=f,
                description=racial_features[f])
            self.session.add(r)
        self.session.commit()

    def insert_armor(self):
        from phb.tool_armor_weapon import armor_types
        for a in armor_types:
            arm = schema.ArmorType(name=a)
            self.session.add(arm)
        self.session.commit()

    def insert_weapon(self):
        from phb.tool_armor_weapon import weapon_types
        for w in weapon_types:
            weapon = schema.WeaponType(name=w)
            self.session.add(weapon)
        self.session.commit()

    def insert_ability_scores(self):
        from phb.abilities_skills_languages import ability_score_types
        for a in ability_score_types:
            ability = schema.AbilityScore(name=a)
            self.session.add(ability)
        self.session.commit()

    def insert_skills(self):
        from phb.abilities_skills_languages import skills
        for s in skills:
            skill = schema.Skill(
                name=s,
                ability=skills[s]
                )
            self.session.add(skill)
        self.session.commit()

    def insert_languages(self):
        from phb.abilities_skills_languages import languages
        for l in languages:
            lang = schema.Language(
                name=l,
                alphabet=languages[l]
                )
            self.session.add(lang)
        self.session.commit()

    def insert_races(self):
        from phb.races import races
        for r in races:
            print('adding race: %s' % r)
            race = schema.Race(
                name=r,
                bonus_str=races[r]['str'],
                bonus_dex=races[r]['dex'],
                bonus_con=races[r]['con'],
                bonus_cha=races[r]['cha'],
                bonus_wis=races[r]['wis'],
                bonus_int=races[r]['int'],
                bonus_choice=races[r]['choice'],
                speed=races[r]['speed'],
                size=races[r]['size']
                )
            for f in races[r]['features']:
                feature = self.session.query(schema.RacialFeature).filter(schema.RacialFeature.name == f).one()
                race.features.append(feature)
            for l in races[r]['languages']:
                lang = self.session.query(schema.Language).filter(schema.Language.name == l).one()
                race.languages.append(lang)
            for p in races[r]['proficiency_skills']:
                prof = self.session.query(schema.Skill).filter(schema.Skill.name == p).one()
                race.proficiency_skills.append(prof)
            for p in races[r]['proficiency_tools']:
                prof = self.session.query(schema.Tool).filter(schema.Tool.name == p).one()
                race.proficiency_tools.append(prof)
            for p in races[r]['proficiency_weapons']:
                prof = self.session.query(schema.WeaponType).filter(schema.WeaponType.name == p).one()
                race.proficiency_weapons.append(prof)
            for p in races[r]['proficiency_armor']:
                prof = self.session.query(schema.ArmorType).filter(schema.ArmorType.name == p).one()
                race.proficiency_armor.append(prof)
            self.session.add(race)
        self.session.commit()

    def query(self, table, field=None, condition=None, original=None):
        """
        Queries the database, obviously. If only a `table` is provided, returns the full table.
        If a `field` and `condition` are supplied, it filters `table` by (`field` like `condition`).
        If an `original` query is provided, it refines that query by (`field` like `condition`).
        Queries are always case-insensitive.
        These are definitely not optimal queries, but the db should never be big enough for it to matter
        and this style fits the most common use case.
        """
        # TODO: improve query logic to take advantage of indexing.
        if field:
            field = self.field_map[table][field]
        if original:
            table = original
        else:
            table = self.session.query(self.table_map[table])
        if type(condition) == str:
            condition = condition.lower()
        if type(field) == tuple:
            result = table.filter(
                field[0].any(sqla.func.lower(field[1]).like('%{}%'.format(condition)))
                )
        elif not field:
            result = table
        else:
            result = table.filter(sqla.func.lower(field).like('%{}%'.format(condition)))
        return result


def recreate_db(filename=None):
    if not filename:
        filename = 'phbbt.db'
    folder = os.path.dirname(os.path.abspath(__file__))
    if os.path.exists(os.path.join(folder, filename)):
        os.remove(os.path.join(folder, filename))
    schema.make_db('sqlite:///'+filename)
    pdb = PhbbtDB('sqlite:///'+filename)
    pdb.insert_ability_scores()
    pdb.insert_armor()
    pdb.insert_feats()
    pdb.insert_languages()
    pdb.insert_racial_features()
    pdb.insert_skills()
    pdb.insert_tools()
    pdb.insert_weapon()
    pdb.insert_races()
    pdb.insert_classes()
    pdb.insert_spells()


class Config:

    def __init__(self):
        pdb = PhbbtDB('sqlite:///phbbt.db')
        self.valid_filters = {
            'spells': {
                'name': None,
                'level': range(10),
                'casters':
                    [i.name for i in pdb.query('jobs', 'spellcasting', 2)] +
                    [i.name for i in pdb.query('jobs', 'spellcasting', 1)],
                'school': (
                    'abjuration',
                    'conjuration',
                    'divination',
                    'enchantment',
                    'evocation',
                    'illusion',
                    'necromancy',
                    'transmutation'
                    ),
                'range': None,
                'casting time': None,
                'duration': None,
                'description': None,
                'scaling': None,
                'ritual': (True, False)
            },
            'jobs': {
                'name': [i.name for i in pdb.query('jobs')],
                'hd': (4, 6, 8, 10, 12, 20),
                'spellcasting': (0, 1, 2),
                'spellcasting ability': ('charisma', 'wisdom', 'intelligence'),
                'tool count': None,
                'tool options': [i.name for i in pdb.query('tools')],
                'class points': (True, False),
                'class point name': [c.class_point_name for c in pdb.query('jobs', 'class points', True)],
                'saves': ('strength', 'dexterity', 'constitution', 'charisma', 'intelligence', 'wisdom'),
                'weapons': [w.name for w in pdb.query('weapons')],
                'armor': [a.name for a in pdb.query('armor')],
                'skills': (
                    'arcana',
                    'survival',
                    'history',
                    'acrobatics',
                    'medicine',
                    'insight',
                    'performance',
                    'deception',
                    'religion',
                    'nature',
                    'intimidation',
                    'investigation',
                    'perception',
                    'stealth',
                    'animal handling',
                    'sleight of hand',
                    'athletics',
                    'persuasion'
                    )
                },
            'races': {
                'name': [r.name for r in pdb.query('races')],
                'str': None,
                'dex': None,
                'con': None,
                'cha': None,
                'wis': None,
                'int': None,
                'choice': None,
                'speed': None,
                'size': ('tiny', 'small', 'medium', 'large', 'huge', 'gargantuan'),
                'features': [f.name for f in pdb.query('features')],
                'languages': [f.name for f in pdb.query('languages')],
                'skills': (
                    'arcana',
                    'survival',
                    'history',
                    'acrobatics',
                    'medicine',
                    'insight',
                    'performance',
                    'deception',
                    'religion',
                    'nature',
                    'intimidation',
                    'investigation',
                    'perception',
                    'stealth',
                    'animal handling',
                    'sleight of hand',
                    'athletics',
                    'persuasion'
                    ),
                'tools': [t.name for t in pdb.query('tools')],
                'weapons': [w.name for w in pdb.query('weapons')],
                'armor': [a.name for a in pdb.query('armor')]
                }
        }
        self.spell_filters = []  # [{'field':'name','value':'rest'}]


if __name__ == '__main__':
    recreate_db('phbbt.db')
    conf = Config()
    pdb = PhbbtDB('sqlite:///phbbt.db')
