from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.tabbedpanel import TabbedPanel
from kivy.uix.popup import Popup
from database import PhbbtDB, Config

conf = Config()
db = PhbbtDB('sqlite:///phbbt.db')


class Phbbt(App):

    def build(self):
        app = AppFrame()
        return app


class ScalingText(GridLayout):

    def __init__(self, text, **kwargs):
        self.text = text
        super(ScalingText, self).__init__(**kwargs)


class SpellView(Popup):

    def __init__(self, spell, **kwargs):
        self.spell = spell
        super(SpellView, self).__init__(**kwargs)
        self.ids.stack.add_widget(ScalingText(text=spell.description), index=1)
        self.ids.stack.add_widget(ScalingText(text=spell.scaling or ''), index=1)


class SpellList(GridLayout):

    def __init__(self, **kwargs):
        super(SpellList, self).__init__(**kwargs)
        self.update_list()
        self.bind(minimum_height=self.setter('height'))

    def update_list(self):
        self.clear_widgets([c for c in self.children if type(c) == SpellListItem])
        spells = db.query('spells')
        for f in conf.spell_filters:
            spells = db.query('spells', field=f['field'], condition=f['value'], original=spells)
        for s in spells:
            self.add_widget(SpellListItem(s), index=1)


class SpellListItem(GridLayout):

    def __init__(self, spell, **kwargs):
        self.spell = spell
        super(SpellListItem, self).__init__(**kwargs)
        self.ids.button.text = self.spell.name

    def show_spell(self):
        pop = SpellView(self.spell)
        pop.open()


class FilterList(GridLayout):

    def __init__(self, **kwargs):
        super(FilterList, self).__init__(**kwargs)
        self.list_filters()

    def list_filters(self):
        print(self.ids)
        #self.ids.current_filters.clear_widgets()
        for i, f in enumerate(conf.spell_filters):
            item = FilterItem(self, i, '{field}: {value}'.format(**f))
            self.ids.current_filters.add_widget(item)

    def add_filter(self):
        pass

    def remove_filter(self, filteritem):
        conf.spell_filters.pop(filteritem.index)
        print(conf.spell_filters)
        self.ids.current_filters.remove_widget(filteritem)


class FilterItem(GridLayout):

    def __init__(self, father, index, desc, **kwargs):
        self.father = father
        self.index = index
        self.description = desc
        super(FilterItem, self).__init__(**kwargs)


class AppFrame(TabbedPanel):
    pass

if __name__ == '__main__':
    Phbbt().run()
