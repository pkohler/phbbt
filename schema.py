import sqlalchemy as sqla
from sqlalchemy.orm import relationship
import sqlalchemy.ext.declarative as dec


Base = dec.declarative_base()

class_armor = sqla.Table(
    'class_armor',
    Base.metadata,
    sqla.Column(
        'class_id',
        sqla.Integer,
        sqla.ForeignKey('player_classes.id')
    ),
    sqla.Column('armor_id', sqla.Integer, sqla.ForeignKey('armor_types.id'))
)

class_saves = sqla.Table(
    'class_saves',
    Base.metadata,
    sqla.Column(
        'class_id',
        sqla.Integer,
        sqla.ForeignKey('player_classes.id')
    ),
    sqla.Column(
        'ability_id',
        sqla.Integer,
        sqla.ForeignKey('ability_scores.id')
    )
)

class_skills = sqla.Table(
    'class_skills',
    Base.metadata,
    sqla.Column(
        'class_id',
        sqla.Integer,
        sqla.ForeignKey('player_classes.id')
    ),
    sqla.Column('skill_id', sqla.Integer, sqla.ForeignKey('skills.id'))
)

class_spells = sqla.Table(
    'class_spells',
    Base.metadata,
    sqla.Column('spell_id', sqla.Integer, sqla.ForeignKey('spells.id')),
    sqla.Column(
        'caster_id',
        sqla.Integer,
        sqla.ForeignKey('player_classes.id')
    )
)

class_tools = sqla.Table(
    'class_tools',
    Base.metadata,
    sqla.Column(
        'class_id',
        sqla.Integer,
        sqla.ForeignKey('player_classes.id')
    ),
    sqla.Column('tool_id', sqla.Integer, sqla.ForeignKey('tools.id'))
)

class_weapons = sqla.Table(
    'class_weapons',
    Base.metadata,
    sqla.Column(
        'class_id',
        sqla.Integer,
        sqla.ForeignKey('player_classes.id')
    ),
    sqla.Column('weapon_id', sqla.Integer, sqla.ForeignKey('weapon_types.id'))
)

race_features = sqla.Table(
    'race_features',
    Base.metadata,
    sqla.Column('race_id', sqla.Integer, sqla.ForeignKey('races.id')),
    sqla.Column(
        'feature_id',
        sqla.Integer,
        sqla.ForeignKey('racial_features.id')
    )
)

race_languages = sqla.Table(
    'race_languages',
    Base.metadata,
    sqla.Column('race_id', sqla.Integer, sqla.ForeignKey('races.id')),
    sqla.Column('language_id', sqla.Integer, sqla.ForeignKey('languages.id'))
)

race_skills = sqla.Table(
    'race_skills',
    Base.metadata,
    sqla.Column('race_id', sqla.Integer, sqla.ForeignKey('races.id')),
    sqla.Column('skill_id', sqla.Integer, sqla.ForeignKey('skills.id'))
)

race_tools = sqla.Table(
    'race_tools',
    Base.metadata,
    sqla.Column('race_id', sqla.Integer, sqla.ForeignKey('races.id')),
    sqla.Column('tool_id', sqla.Integer, sqla.ForeignKey('tools.id'))
)

race_weapons = sqla.Table(
    'race_weapons',
    Base.metadata,
    sqla.Column('race_id', sqla.Integer, sqla.ForeignKey('races.id')),
    sqla.Column('weapon_id', sqla.Integer, sqla.ForeignKey('weapon_types.id'))
)

race_armor = sqla.Table(
    'race_armor',
    Base.metadata,
    sqla.Column('race_id', sqla.Integer, sqla.ForeignKey('races.id')),
    sqla.Column('armor_id', sqla.Integer, sqla.ForeignKey('armor_types.id'))
)

feat_abilities = sqla.Table(
    'feat_abilities',
    Base.metadata,
    sqla.Column('feat_id', sqla.Integer, sqla.ForeignKey('feats.id')),
    sqla.Column(
        'ability_id',
        sqla.Integer,
        sqla.ForeignKey('ability_scores.id')
    )
)

feat_prereq_type_link = sqla.Table(
    'feat_prereq_type_link',
    Base.metadata,
    sqla.Column(
        'feat_prereq_id',
        sqla.Integer,
        sqla.ForeignKey('feat_prerequisites.id')
    ),
    sqla.Column(
        'feat_prereq_type_id',
        sqla.Integer,
        sqla.ForeignKey('feat_prerequisite_type.id')
    )
)


class AbilityScore(Base):

    __tablename__ = 'ability_scores'
    id = sqla.Column(sqla.Integer, primary_key=True)
    name = sqla.Column(sqla.String, nullable=False, unique=True)


class ArmorType(Base):

    __tablename__ = 'armor_types'
    id = sqla.Column(sqla.Integer, primary_key=True)
    name = sqla.Column(sqla.String, nullable=False, unique=True)


class Feat(Base):

    __tablename__ = 'feats'
    id = sqla.Column(sqla.Integer, primary_key=True)
    name = sqla.Column(sqla.String, nullable=False, unique=True)
    ability_number = sqla.Column(sqla.Integer)
    ability_choices = relationship(
        'AbilityScore',
        secondary=feat_abilities,
        backref='feats'
    )
    prereq_group = sqla.Column(sqla.Integer, sqla.ForeignKey('FeatPrereqGroup'))
    description = sqla.Column(sqla.String, nullable=False)


class FeatPrereqGroup(Base):

    __tablename__ = 'feat_prerequisite_groups'
    id = sqla.Column(sqla.Integer, primary_key=True)
    feat_id = sqla.Column(sqla.Integer, sqla.ForeignKey('feats.id'))
    parent_group_id = sqla.Column(
        sqla.Integer,
        sqla.ForeignKey('feat_prerequisite_groups.id')
    )
    bool_type = sqla.Column(sqla.Boolean, nullable=False)  # AND or OR
    feat_prereq_type = sqla.Column(
        sqla.Integer,
        sqla.ForeignKey('feat_prerequisite_types.id'),
        nullable=False
    )


class FeatPrereqType(Base):

    __tablename__ = 'feat_prerequisite_types'
    id = sqla.Column(sqla.Integer, primary_key=True)
    fpt = sqla.Column(sqla.String, nullable=False, unique=True)
    # possibilities: ['ability_score', 'proficiency', 'prereq_group', 'spellcasting']


class FeatPrereq(Base):

    __tablename__ = 'feat_prerequisites'
    id = sqla.Column(sqla.Integer, primary_key=True)
    prereq_group = sqla.Column(sqla.Integer, sqla.ForeignKey('feat_prerequisite_groups.id'))
    prereq_proficiency = sqla.Column(sqla.Integer, sqla.ForeignKey('proficiencies.id'))  
    prereq_ability = sqla.Column(sqla.Integer, sqla.ForeignKey('ability_scores.id'))
    prereq_min_val = sqla.Column(sqla.Integer)  # for use with ability prereqs


class Language(Base):

    __tablename__ = 'languages'
    id = sqla.Column(sqla.Integer, primary_key=True)
    name = sqla.Column(sqla.String, nullable=False, unique=True)
    alphabet = sqla.Column(sqla.String)


class Proficiency(Base):

    __tablename__ = 'proficiencies'
    id = sqla.Column(sqla.Integer, primary_key=True)
    # proficiency type can be one of ['spellcasting', 'language', 'weapon',
    # 'armor', 'tool']
    prof_type = sqla.Column(sqla.String, nullable=False, unique=True)
    caster_class_link = sqla.Column(sqla.Integer, sqla.ForeignKey('player_classes.id'))
    caster_ability_link = sqla.Column(sqla.Integer, sqla.ForeignKey('ability_scores.id'))
    weapon_link = sqla.Column(sqla.Integer, sqla.ForeignKey('weapon_types.id'))
    armor_link = sqla.Column(sqla.Integer, sqla.ForeignKey('armor_types.id'))
    language_link = sqla.Column(sqla.Integer, sqla.ForeignKey('languages.id'))


class PlayerClass(Base):

    __tablename__ = 'player_classes'
    id = sqla.Column(sqla.Integer, primary_key=True)
    name = sqla.Column(sqla.String, nullable=False, unique=True)
    hd = sqla.Column(sqla.Integer, nullable=False)
    skill_choices = sqla.Column(sqla.Integer, nullable=False)
    # spellcasting grants 1/value caster levels per class level
    # spellcasting=0 means the class doesn't have spellcasting.
    spellcasting = sqla.Column(sqla.Integer, nullable=False)
    spellcasting_ability = sqla.Column(sqla.Integer, sqla.ForeignKey('ability_scores.id'))
    tool_choices = sqla.Column(sqla.Integer, nullable=False)
    class_points = sqla.Column(sqla.Boolean, nullable=False)
    class_point_name = sqla.Column(sqla.String)
    proficiency_skills = relationship(
        'Skill',
        secondary=class_skills,
        backref='class_associations'
    )
    proficiency_tools = relationship(
        'Tool',
        secondary=class_tools,
        backref='class_associations'
    )
    proficiency_weapons = relationship(
        'WeaponType',
        secondary=class_weapons,
        backref='class_associations'
    )
    proficiency_armor = relationship(
        'ArmorType',
        secondary=class_armor,
        backref='class_associations'
    )
    proficiency_saves = relationship(
        'AbilityScore',
        secondary=class_saves,
        backref='class_associations'
    )


class ClassFeature(Base):

    __tablename__='class_feature'
    id = sqla.Column(sqla.Integer, primary_key=True)
    playerclass_id = sqla.Column(
        sqla.Integer,
        sqla.ForeignKey('player_classes.id')
    )
    level_acquired = sqla.Column(sqla.Integer, nullable=False)
    name = sqla.Column(sqla.String, nullable=False)
    description = sqla.Column(sqla.String, nullable=False)


class Race(Base):

    __tablename__ = 'races'
    id = sqla.Column(sqla.Integer, primary_key=True)
    name = sqla.Column(sqla.String, nullable=False, unique=True)
    bonus_str = sqla.Column(sqla.Integer)
    bonus_dex = sqla.Column(sqla.Integer)
    bonus_con = sqla.Column(sqla.Integer)
    bonus_cha = sqla.Column(sqla.Integer)
    bonus_wis = sqla.Column(sqla.Integer)
    bonus_int = sqla.Column(sqla.Integer)
    bonus_choice = sqla.Column(sqla.Integer)
    speed = sqla.Column(sqla.Integer, nullable=False)
    size = sqla.Column(sqla.String, nullable=False)
    features = relationship(
        'RacialFeature',
        secondary=race_features,
        backref='racial_associations'
    )
    languages = relationship(
        'Language',
        secondary=race_languages,
        backref='racial_associations'
    )
    proficiency_skills = relationship(
        'Skill',
        secondary=race_skills,
        backref='racial_associations'
    )
    proficiency_tools = relationship(
        'Tool',
        secondary=race_tools,
        backref='racial_associations'
    )
    proficiency_weapons = relationship(
        'WeaponType',
        secondary=race_weapons,
        backref='racial_associations'
    )
    proficiency_armor = relationship(
        'ArmorType',
        secondary=race_armor,
        backref='racial_associations'
    )


class RacialFeature(Base):

    __tablename__ = 'racial_features'
    id = sqla.Column(sqla.Integer, primary_key=True)
    name = sqla.Column(sqla.String, nullable=False, unique=True)
    description = sqla.Column(sqla.String, nullable=False)


class Skill(Base):

    __tablename__ = 'skills'
    id = sqla.Column(sqla.Integer, primary_key=True)
    name = sqla.Column(sqla.String, nullable=False, unique=True)
    ability = sqla.Column(sqla.String)


class Spell(Base):

    __tablename__ = 'spells'
    id = sqla.Column(sqla.Integer, primary_key=True)
    name = sqla.Column(sqla.String, nullable=False, unique=True)
    level = sqla.Column(sqla.Integer, nullable=False)
    school = sqla.Column(sqla.String, nullable=False)
    ritual = sqla.Column(sqla.Boolean, nullable=False)
    range = sqla.Column(sqla.String, nullable=False)
    components = sqla.Column(sqla.String, nullable=False)
    duration = sqla.Column(sqla.String, nullable=False)
    scaling = sqla.Column(sqla.String)
    description = sqla.Column(sqla.String, nullable=False)
    casting_time = sqla.Column(sqla.String, nullable=False)
    casters = relationship(
        'PlayerClass',
        secondary=class_spells,
        backref='spells'
    )


class Tool(Base):

    __tablename__ = 'tools'
    id = sqla.Column(sqla.Integer, primary_key=True)
    name = sqla.Column(sqla.String, nullable=False, unique=True)


class WeaponType(Base):

    __tablename__ = 'weapon_types'
    id = sqla.Column(sqla.Integer, primary_key=True)
    name = sqla.Column(sqla.String, nullable=False, unique=True)


def make_db(filename):
    engine = sqla.create_engine(filename)
    Base.metadata.bind = engine
    Base.metadata.create_all(engine)
