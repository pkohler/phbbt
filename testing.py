from database import PhbbtDB, recreate_db


def show_spell(spell_obj):
    out = '''    Name:    {}\n    Level:    {}\n    Casters:    {}\n    School:    {}
    Components:    {}\n    Range:    {}\n    Casting Time:    {}\n    Duration:    {}
    Description:    {}\n    Scaling:    {}\n    Ritual:    {}\n'''.format(
        spell_obj.name.capitalize(),
        spell_obj.level,
        ', '.join([c.name.capitalize() for c in spell_obj.casters]),
        spell_obj.school.capitalize(),
        spell_obj.components,
        spell_obj.range.capitalize(),
        spell_obj.casting_time.capitalize(),
        spell_obj.duration.capitalize(),
        spell_obj.description,
        spell_obj.scaling,
        spell_obj.ritual)
    print(out)


def show_job(job):
    out = """    Class:    {}\n    Hit die:    {}\n    Number of skills:    {}
    Skill options:    {}\n    Save proficiencies:    {}\n""".format(
        job.name.capitalize(),
        job.hd,
        job.skill_choices,
        ', '.join(s.name.capitalize() for s in job.proficiency_skills),
        ', '.join(s.name.capitalize() for s in job.proficiency_saves)
        )
    if job.tool_choices:
        out += "    Number of tools:    {}\n    Tool options:    {}\n".format(
            job.tool_choices,
            ', '.join(t.name.capitalize() for t in job.proficiency_tools)
            )
    if job.proficiency_weapons:
        out += "    Weapon proficiencies:    {}\n".format(
            ', '.join(w.name.capitalize() for w in job.proficiency_weapons)
            )
    if job.proficiency_armor:
        out += "    Armor proficiencies:    {}\n".format(
            ', '.join(a.name.capitalize() for a in job.proficiency_armor)
            )
    if job.spellcasting:
        out += '    Spellcasting ability:    {}\n'.format(
            job.spellcasting_ability.capitalize()
            )
    if job.class_points:
        out += '    Class gains {} points (1/level)\n'.format(
            job.class_point_name.capitalize()
            )
    print(out)


def show_race(race):
    out = """    Race:     {}
    Stats:     STR +{}, DEX + {}, CON +{}, INT +{}, WIS +{}, CHA +{}, Choice: {}
    Size:      {}
    Speed:     {}
    Languages: {}
    Skills:    {}
    Tools:     {}
    Weapons:   {}
    Armor:     {}
    Features:  {}""".format(
        race.name.capitalize(),
        race.bonus_str,
        race.bonus_dex,
        race.bonus_con,
        race.bonus_int,
        race.bonus_wis,
        race.bonus_cha,
        race.bonus_choice,
        race.size.capitalize(),
        race.speed,
        ', '.join(l.name.capitalize() for l in race.languages),
        ', '.join(s.name.capitalize() for s in race.proficiency_skills),
        ', '.join(t.name.capitalize() for t in race.proficiency_tools),
        ', '.join(w.name.capitalize() for w in race.proficiency_weapons),
        ', '.join(a.name.capitalize() for a in race.proficiency_armor),
        '\n            '.join(['%s: %s' % (f.name, f.description) for f in race.features])
        )
    print(out)


if __name__ == '__main__':
    recreate_db()
    db = PhbbtDB()
    q = db.query('spells', 'name', 'chill')
    for r in q:
        show_spell(r)
